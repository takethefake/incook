import Joi from 'joi';

export default {
  // POST /api/dishes
  createDish: {
    body: {
      name: Joi.string().required(),
      description: Joi.string().allow(''),
      cookingDate: Joi.date().required(),
      maxParticipants: Joi.number().required(),
      link: Joi.string().allow('')
    }
  },

  // UPDATE /api/dishes/:dishId
  updateDish: {
    body: {
      cookingDate: Joi.date(),
      maxParticipants: Joi.number(),
      link: Joi.string().allow(''),
      name: Joi.string(),
      description: Joi.string().allow('')
    },
    params: {
      dishId: Joi.string()
        .hex()
        .required()
    }
  },

  // POST /api/dishes/:dishId/join
  joinDish: {
    params: {
      dishId: Joi.string()
        .hex()
        .required()
    }
  }
};

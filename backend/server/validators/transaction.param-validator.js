import Joi from 'joi';

export default {
  // POST /api/dishes
  createTransaction: {
    body: {
      to: Joi.string().required(),
      amount: Joi.number().required()
    }
  }
};

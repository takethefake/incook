import httpStatus from 'http-status';
import APIError from '../helpers/APIError';
import Dish from '../models/dish.model';
import { searchRecipieImage } from '../library/chefkoch.api';
import Transaction from '../models/transactions.model';

/**
 * Load dish and append to req.
 */
function load(req, res, next, id) {
  Dish.get(id)
    .then((dish) => {
      req.dish = dish; // eslint-disable-line no-param-reassign
      return next();
    })
    .catch(e => next(e));
}

/**
 * Get dish
 * @returns {dish}
 */
function get(req, res) {
  return res.json(req.dish);
}

/**
 * Create new dish
 * @property {string} req.body.name - The name of dish.
 * @returns {dish}
 */
function create(req, res, next) {
  const user = req.user;
  const createDish = (imageLink) => {
    const dishData = {
      cookingDate: req.body.cookingDate,
      maxParticipants: req.body.maxParticipants,
      name: req.body.name,
      description: req.body.description,
      participants: [user.id],
      cook: user.id,
      link: imageLink,
      price: req.body.price
    };
    const dish = new Dish(dishData);

    dish
      .save()
      .then(saveddish => res.json(saveddish))
      .catch(e => next(e));
  };
  searchRecipieImage(req.body.name)
    .then(createDish)
    .catch(() => createDish(''));
}

/**
 * Update existing dish
 * @property {string} req.body.name - The name of dish.
 * @returns {dish}
 */
function update(req, res, next) {
  const dish = req.dish;
  const updateDish = (imageLink) => {
    if (req.body.name) {
      dish.name = req.body.name;
    }
    if (req.body.description) {
      dish.description = req.body.description;
    }
    if (req.body.link && req.body.link !== '') {
      dish.link = req.body.link;
    } else {
      dish.link = imageLink;
    }
    if (req.body.cookingDate) {
      dish.cookingDate = req.body.cookingDate;
    }
    if (req.body.maxParticipants) {
      if (req.body.participants.length > req.body.maxParticipants) {
        const err = new APIError(
          "Can't decrease maxParticipants to lower than currently enrolled!",
          httpStatus.BAD_REQUEST
        );
        throw err;
      }
      dish.maxParticipants = req.body.maxParticipants;
    }

    dish
      .save()
      .then(savedDish => res.json(savedDish))
      .catch(e => next(e));
  };

  if (dish.cook.id === req.user.id) {
    searchRecipieImage(req.body.name)
      .then(updateDish)
      .catch(() => updateDish(''));
  } else {
    const err = new APIError('Only the Cook is able to change the dish!', httpStatus.UNAUTHORIZED);
    throw err;
  }
}

function join(req, res, next) {
  const dish = req.dish;
  if (dish.participants.length === dish.maxParticipants) {
    const err = new APIError('All Places are occupied', httpStatus.FORBIDDEN);
    throw err;
  }

  /* Transaction.getPointsForUser(req.user.id).then((points) => {
    if (dish.price > points) {
      throw new APIError(
        'You have not enough Points to do this Transaction',
        httpStatus.BAD_REQUEST
      );
    }*/

  const indexOfParticipant = dish.participants
    .map(participant => participant.id)
    .indexOf(req.user.id);

  if (indexOfParticipant >= 0) {
    const err = new APIError('You are already joining the Dish', httpStatus.FORBIDDEN);
    throw err;
  }
  dish.participants.push(req.user.id);
  dish
    .save()
    .then((savedDish) => {
      const transactionData = {
        from: req.user.id,
        to: savedDish.cook.id,
        amount: savedDish.price
      };
      const trans = new Transaction(transactionData);
      return trans
        .save()
        .then(savedTransaction => res.json({ dish: savedDish, trans: savedTransaction }));
    })
    .catch(e => next(e));
  /* });*/
}

function leave(req, res, next) {
  const dish = req.dish;
  if (dish.cook.id === req.user.id) {
    const err = new APIError("The cook can't leave the dish", httpStatus.FORBIDDEN);
    throw err;
  }
  const indexOfParticipant = dish.participants
    .map(participant => participant.id)
    .indexOf(req.user.id);

  if (indexOfParticipant >= 0) {
    dish.participants.splice(indexOfParticipant, 1);
    dish
      .save()
      .then((savedDish) => {
        const transactionData = {
          to: req.user.id,
          from: savedDish.cook.id,
          amount: savedDish.price
        };
        const trans = new Transaction(transactionData);

        return trans
          .save()
          .then(savedTransaction => res.json({ dish: savedDish, trans: savedTransaction }));
      })
      .catch(e => next(e));
  } else {
    const err = new APIError("You can't leave a Dish you aren't joining", httpStatus.FORBIDDEN);
    throw err;
  }
}

/**
 * Get dish list.
 * @property {number} req.query.skip - Number of dishs to be skipped.
 * @property {number} req.query.limit - Limit number of dishs to be returned.
 * @returns {dish[]}
 */
function list(req, res, next) {
  const { limit = 50, skip = 0 } = req.query;
  Dish.list({ limit, skip })
    .then(dishes => res.json(dishes))
    .catch(e => next(e));
}

/**
 * Delete dish.
 * @returns {dish}
 */
function remove(req, res, next) {
  const dish = req.dish;
  dish
    .remove()
    .then(deleteddish => res.json(deleteddish))
    .catch(e => next(e));
}

export default { load, get, create, update, list, remove, join, leave };

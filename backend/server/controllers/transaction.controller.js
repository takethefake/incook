import httpStatus from 'http-status';

import Transaction from '../models/transactions.model';
import Dish from '../models/dish.model';
import User from '../models/user.model';
import APIError from '../helpers/APIError';

/**
 * Load transaction and append to req.
 */
function load(req, res, next, id) {
  Transaction.get(id)
    .then((transaction) => {
      req.transaction = transaction; // eslint-disable-line no-param-reassign
      return next();
    })
    .catch(e => next(e));
}

/**
 * Get transaction
 * @returns {transaction}
 */
function get(req, res) {
  return res.json(req.transaction);
}

/**
 * Get transaction list.
 * @property {number} req.query.skip - Number of transactios to be skipped.
 * @property {number} req.query.limit - Limit number of transactions to be returned.
 * @returns {transaction[]}
 */
function list(req, res, next) {
  const { limit = 0, skip = 0 } = req.query;
  Transaction.list({ limit, skip })
    .then(transactions => res.json(transactions))
    .catch(e => next(e));
}

function migrate(req, res, next) {
  const { limit = 50, skip = 0 } = req.query;
  Dish.list({ limit, skip }).then(dishes =>
    res.json(
      dishes.map((dish) => {
        dish.amount = 1;
        return dish.save().then(savedDish =>
          savedDish.participants.map((participant) => {
            const transactionData = {
              from: participant.id,
              to: savedDish.cook.id,
              amount: savedDish.amount,
              createdAt: savedDish.cookingDate
            };
            const trans = new Transaction(transactionData);
            return trans.save().then(savedTrans => savedTrans);
          })
        );
      })
    )
  );
}

/**
 * Create new transaction
 * @property {string} req.body.name - The name of transaction.
 * @returns {transaction}
 */
function create(req, res, next) {
  const user = req.user;
  const { amount, to } = req.body;
  if (amount <= 0) {
    throw new APIError("The Amount can't be lower than 0", httpStatus.BAD_REQUEST);
  } else {
    Transaction.getPointsForUser(user.id)
      .then((points) => {
        if (amount > points) {
          throw new APIError(
            'You have not enough Points to do this Transaction',
            httpStatus.BAD_REQUEST
          );
        } else {
          User.getByUsername(to)
            .then((validUser) => {
              const transactionData = {
                to: validUser.id,
                amount,
                from: user.id
              };

              const transaction = new Transaction(transactionData);

              transaction
                .save()
                .then(savedtransaction => res.json(savedtransaction))
                .catch(e => next(e));
            })
            .catch(e => next(e));
        }
      })
      .catch(e => next(e));
  }
}

export default { load, get, list, migrate, create };

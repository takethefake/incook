import httpStatus from 'http-status';
import User from '../models/user.model';
import Auth from './auth.controller';
import APIError from '../helpers/APIError';

/**
 * Load user and append to req.
 */
function load(req, res, next, id) {
  User.get(id)
    .then((user) => {
      req.userInRequest = user; // eslint-disable-line no-param-reassign
      return next();
    })
    .catch(e => next(e));
}

/**
 * Get user
 * @returns {User}
 */
function get(req, res) {
  res.json(req.userInRequest);
}

/**
 * Create new user
 * @property {string} req.body.username - The username of user.
 * @returns {User}
 */
function create(req, res, next) {
  User.getByUsername(req.body.username)
    .then(() => {
      next(new APIError('A User with this username already exists', httpStatus.BAD_REQUEST));
    })
    .catch(() => {
      Auth.hashPassword(req.body.password).then((hash) => {
        const user = new User({
          username: req.body.username,
          password: hash
        });

        // TODO change this to get user by model
        user
          .save()
          .then((savedUser) => {
            const curUser = savedUser;
            delete curUser.password;
            res.json(curUser);
          })
          .catch(e => next(e));
      });
    });
}

/**
 * Update existing user
 * @property {string} req.body.username - The username of user.
 * @returns {User}
 */
function update(req, res, next) {
  const user = req.userInRequest;
  if (user.id === req.user.id) {
    Auth.hashPassword(req.body.password).then((hash) => {
      user.username = req.body.username;
      user.password = hash;

      // TODO change this to get user by model
      user
        .save()
        .then((savedUser) => {
          const curUser = savedUser;
          delete curUser.password;
          res.json(curUser);
        })
        .catch(e => next(e));
    });
  } else {
    next(new APIError('You are only allowed to change yourself', httpStatus.UNAUTHORIZED));
  }
}

/**
 * Get user list.
 * @property {number} req.query.skip - Number of users to be skipped.
 * @property {number} req.query.limit - Limit number of users to be returned.
 * @returns {User[]}
 */
function list(req, res, next) {
  const { limit = 50, skip = 0 } = req.query;
  User.list({ limit, skip })
    .then(users => res.json(users))
    .catch(e => next(e));
}

/**
 * Delete user.
 * @returns {User}
 */
function remove(req, res, next) {
  const user = req.userInRequest;
  if (user.id === req.user.id) {
    user
      .remove()
      .then(deletedUser => res.json(deletedUser))
      .catch(e => next(e));
  } else {
    next(new APIError('You are only allowed to delete yourself', httpStatus.UNAUTHORIZED));
  }
}

export default { load, get, create, update, list, remove };

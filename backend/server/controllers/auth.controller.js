import jwt from 'jsonwebtoken';
import httpStatus from 'http-status';
import bcrypt from 'bcrypt';

import APIError from '../helpers/APIError';
import config from '../../config/config';
import User from '../models/user.model';

const hashPassword = plaintextPassword =>
  new Promise((resolve) => {
    bcrypt.hash(plaintextPassword, 10).then(hash => resolve(hash));
  });

const checkPassword = (plaintextPassword, hash) =>
  new Promise((resolve) => {
    bcrypt.compare(plaintextPassword, hash).then(isEqual => resolve(isEqual));
  });

/**
 * Returns jwt token if valid username and password is provided
 * @param req
 * @param res
 * @returns {*}
 */
function login(req, res, next) {
  User.getByUsername(req.body.username)
    .then(user => checkPassword(req.body.password, user.password).then((passwordIsValid) => {
      if (passwordIsValid) {
        const token = jwt.sign(
          {
            username: user.username,
            id: user.id
          },
            config.jwtSecret
          );
        res.json({
          token,
          id: user.id,
          username: user.username
        });
        return res;
      }
      throw new APIError('Password is invalid', httpStatus.NOT_FOUND);
    }))
    .catch(err => next(err));
}

export default { login, hashPassword, checkPassword };

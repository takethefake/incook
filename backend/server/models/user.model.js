import Promise from 'bluebird';
import mongoose from 'mongoose';
import httpStatus from 'http-status';
import APIError from '../helpers/APIError';
import Transaction from './transactions.model';

/**
 * User Schema
 */
const UserSchema = new mongoose.Schema(
  {
    username: {
      type: String,
      required: true
    },
    password: {
      type: String,
      required: true
    },
    createdAt: {
      type: Date,
      default: Date.now
    }
  },
  {
    toObject: {
      virtuals: true
    },
    toJSON: {
      virtuals: true
    }
  }
);

/**
 * Add your
 * - pre-save hooks
 * - validations
 * - virtuals
 */

/**

 * Methods
 */
UserSchema.method({});

/**
 * Statics
 */
UserSchema.statics = {
  /**
   * Get user
   * @param {ObjectId} id - The objectId of user.
   * @returns {Promise<User, APIError>}
   */
  get(id) {
    return this.findById(id)
      .select('-password')
      .exec()
      .then((user) => {
        if (user) {
          return Transaction.getPointsForUser(user.id).then((points) => {
            const objUser = user.toObject();
            objUser.dishCount = points;
            return objUser;
          });
        }
        throw new APIError('No such user exists!', httpStatus.NOT_FOUND);
      });
  },

  getByUsername(username) {
    return this.findOne({ username: new RegExp(username, 'i') })
      .exec()
      .then((user) => {
        if (user) {
          return user;
        }
        throw new APIError('No such user exists!', httpStatus.NOT_FOUND);
      });
  },

  /**
   * List users in descending order of 'createdAt' timestamp.
   * @param {number} skip - Number of users to be skipped.
   * @param {number} limit - Limit number of users to be returned.
   * @returns {Promise<User[]>}
   */
  list({ skip = 0, limit = 50 } = {}) {
    return this.find()
      .select('-password')
      .sort({ createdAt: -1 })
      .skip(+skip)
      .limit(+limit)
      .exec()
      .then(userList =>
        Promise.all(
          userList.map(user =>
            Transaction.getPointsForUser(user.id).then((points) => {
              const objUser = user.toObject();
              objUser.dishCount = points;
              console.log(objUser);
              return objUser;
            })
          )
        )
      );
  }
};

/**
 * @typedef User
 */
export default mongoose.model('User', UserSchema);

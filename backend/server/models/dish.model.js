import Promise from 'bluebird';
import mongoose from 'mongoose';
import httpStatus from 'http-status';
import APIError from '../helpers/APIError';

/**
 * dish Schema
 */
const DishSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true
  },
  description: {
    type: String
  },
  createdAt: {
    type: Date,
    default: Date.now
  },
  cookingDate: {
    type: Date,
    required: true
  },
  cook: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
    required: true
  },
  price: {
    type: Number,
    required: true
  },
  link: {
    type: String
  },
  maxParticipants: {
    type: Number,
    required: true
  },
  participants: [
    {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'User'
    }
  ]
});

/**
 * Add your
 * - pre-save hooks
 * - validations
 * - virtuals
 */

/**
 * Methods
 */
DishSchema.method({});

/**
 * Statics
 */
DishSchema.statics = {
  /**
   * Get dish
   * @param {ObjectId} id - The objectId of dish.
   * @returns {Promise<dish, APIError>}
   */
  get(id) {
    return this.findById(id)
      .populate('cook', { username: 1 })
      .populate('participants', { username: 1 })
      .exec()
      .then((dish) => {
        if (dish) {
          return dish;
        }
        const err = new APIError('No such dish exists!', httpStatus.NOT_FOUND);
        return Promise.reject(err);
      });
  },

  getDishCountForUser(userId) {
    return this.find({ cook: userId })
      .exec()
      .then((dishesCooked) => {
        const timesCooked = dishesCooked.reduce(
          (currentCount, dish) => (currentCount += dish.participants.length - 1),
          0
        );
        const objId = new mongoose.Types.ObjectId(userId);
        return this.count({
          cook: { $ne: userId },
          participants: userId
        }).then((timesParticipated) => {
          const cookingDiff = timesCooked - timesParticipated;
          return cookingDiff;
        });
      });
  },

  /**
   * List dishs in descending order of 'createdAt' timestamp.
   * @param {number} skip - Number of dishs to be skipped.
   * @param {number} limit - Limit number of dishs to be returned.
   * @returns {Promise<dish[]>}
   */
  list({ skip = 0, limit = 50 } = {}) {
    return this.find()
      .populate('cook', { username: 1 })
      .populate('participants', { username: 1 })
      .sort({ createdAt: -1 })
      .skip(+skip)
      .limit(+limit)
      .exec();
  }
};

/**
 * @typedef dish
 */
export default mongoose.model('Dish', DishSchema);

import Promise from 'bluebird';
import mongoose from 'mongoose';
import httpStatus from 'http-status';
import Float from 'mongoose-float';
import APIError from '../helpers/APIError';

Float.loadType(mongoose);

/**
 * transaction Schema
 */
const TransactionSchema = new mongoose.Schema({
  createdAt: {
    type: Date,
    default: Date.now
  },
  from: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
    required: true
  },
  to: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
    required: true
  },
  amount: {
    type: Float,
    required: true
  }
});

/**
 * Add your
 * - pre-save hooks
 * - validations
 * - virtuals
 */

/**
 * Methods
 */
TransactionSchema.method({});

/**
 * Statics
 */
TransactionSchema.statics = {
  /**
   * Get transaction
   * @param {ObjectId} id - The objectId of transaction.
   * @returns {Promise<transaction, APIError>}
   */
  get(id) {
    return this.findById(id)
      .populate('from', { username: 1 })
      .populate('to', { username: 1 })
      .exec()
      .then((transaction) => {
        if (transaction) {
          return transaction;
        }
        const err = new APIError('No such transaction exists!', httpStatus.NOT_FOUND);
        return Promise.reject(err);
      });
  },

  getPointsForUser(userId) {
    return this.find({ from: userId })
      .exec()
      .then((transactionsSend) => {
        const sentPoints = transactionsSend.reduce(
          (currentCount, transaction) => (currentCount -= transaction.amount),
          0
        );
        return this.find({
          to: userId
        }).then((transactionsReceived) => {
          const receivedPoints = transactionsReceived.reduce(
            (currentCount, transaction) => (currentCount += transaction.amount),
            sentPoints
          );
          return receivedPoints;
        });
      });
  },

  /**
   * List transactionss in descending order of 'createdAt' timestamp.
   * @param {number} skip - Number of dishs to be skipped.
   * @param {number} limit - Limit number of transactions to be returned.
   * @returns {Promise<transaction[]>}
   */
  list({ skip = 0, limit = 300 } = {}) {
    return this.find()
      .populate('from', { username: 1 })
      .populate('to', { username: 1 })
      .sort({ createdAt: -1 })
      .skip(+skip)
      .limit(+limit)
      .exec();
  }
};

/**
 * @typedef dish
 */
export default mongoose.model('Transaction', TransactionSchema);

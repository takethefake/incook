const fetch = require('node-fetch');

export const searchRecipieImage = recipeName =>
  new Promise((resolve, reject) => {
    const url = `https://api.chefkoch.de/v2/recipes?query=${recipeName}&hasImage=true`;
    fetch(url)
      .then((recipeResult) => {
        recipeResult
          .text()
          .then(JSON.parse)
          .then((recipeObj) => {
            if (recipeObj.count > 0) {
              const recipeId = recipeObj.results[0].recipe.id;
              const imageUrl = `https://api.chefkoch.de/v2/recipes/${recipeId}/images`;
              fetch(imageUrl)
                .then((imageResult) => {
                  imageResult
                    .text()
                    .then(JSON.parse)
                    .then((imageObj) => {
                      if (imageObj.count > 0) {
                        const link = `https://api.chefkoch.de/v2/recipes/${recipeId}/images/${
                          imageObj.results[0].id
                        }/fit-960x720`;
                        resolve(link);
                      }
                    });
                })
                .catch((err) => {
                  reject(err);
                });
            } else {
              reject(null);
            }
          });
      })
      .catch((err) => {
        reject(err);
      });
  });

export default {
  searchRecipieImage
};

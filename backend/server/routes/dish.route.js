import express from 'express';
import validate from 'express-validation';
import expressJwt from 'express-jwt';
import paramValidation from '../validators/dish.param-validator';
import dishCtrl from '../controllers/dish.controller';
import config from '../../config/config';

const router = express.Router(); // eslint-disable-line new-cap

router
  .route('/')
  /** GET /api/dishes - Get list of dishes */
  .get(dishCtrl.list)
  /** POST /api/dishes - Create new dish */
  .post(
    expressJwt({ secret: config.jwtSecret }),
    validate(paramValidation.createDish),
    dishCtrl.create
  );

router
  .route('/:dishId')
  /** GET /api/users/:userId - Get dish */
  .get(expressJwt({ secret: config.jwtSecret }), dishCtrl.get)
  /** PUT /api/users/:dishId - Update dish */
  .put(
    expressJwt({ secret: config.jwtSecret }),
    validate(paramValidation.updateDish),
    dishCtrl.update
  )
  /** DELETE /api/users/:dishId - Delete dish */
  .delete(expressJwt({ secret: config.jwtSecret }), dishCtrl.remove);

router
  .route('/:dishId/join')
  .post(expressJwt({ secret: config.jwtSecret }), validate(paramValidation.joinDish), dishCtrl.join)
  .delete(expressJwt({ secret: config.jwtSecret }), dishCtrl.leave);

/** Load user when API with userId route parameter is hit */
router.param('dishId', dishCtrl.load);

export default router;

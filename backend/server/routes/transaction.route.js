import express from 'express';
import validate from 'express-validation';
import expressJwt from 'express-jwt';
import paramValidation from '../validators/transaction.param-validator';
import transactionCtrl from '../controllers/transaction.controller';
import config from '../../config/config';

const router = express.Router(); // eslint-disable-line new-cap

router
  .route('/')
  /** GET /api/transactions - Get list of users */
  .get(expressJwt({ secret: config.jwtSecret }), transactionCtrl.list)
  .post(
    expressJwt({ secret: config.jwtSecret }),
    validate(paramValidation.createTransaction),
    transactionCtrl.create
  );

router
  .route('/migrate')
  /** GET /api/transactions - Get list of users */
  .get(expressJwt({ secret: config.jwtSecret }), transactionCtrl.migrate);

router
  .route('/:transactionId')
  /** GET /api/transactions/:transactionId - Get transaction */
  .get(expressJwt({ secret: config.jwtSecret }), transactionCtrl.get);

/** Load user when API with userId route parameter is hit */
router.param('transactionId', transactionCtrl.load);

export default router;

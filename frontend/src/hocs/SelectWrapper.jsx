import { Component } from 'react';
import PropTypes from 'prop-types';
import { ID_KEY } from '../actions/commons';

export default class SelectWrapper extends Component {
    static propTypes = {
        multi: PropTypes.bool,
        children: PropTypes.func.isRequired,
        selectedItems: PropTypes.arrayOf(PropTypes.any),
        // eslint-disable-next-line react/forbid-prop-types
        selectedItem: PropTypes.any,
        onChange: PropTypes.func,
    };

    static defaultProps = {
        multi: false,
        selectedItem: null,
        selectedItems: [],
        onChange: () => undefined,
    };

    constructor(props) {
        super(props);
        this.state = {
            selectedItems: props.selectedItems, // used for multi select
            selectedItem: props.selectedItem, // used for single select
        };
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.selectedItem !== this.props.selectedItem) {
            this.setState({ selectedItem: nextProps.selectedItem });
        }

        if (nextProps.selectedItems !== this.props.selectedItems) {
            this.setState({ selectedItems: nextProps.selectedItems });
        }
    }

    onMultiSelect = (selectedItem) => {
        this.setState(({ selectedItems }) => {
            const index = selectedItems.findIndex(item =>
                selectedItem === item ||
                (selectedItem[ID_KEY] && item[ID_KEY] && selectedItem[ID_KEY] === item[ID_KEY]),
            );

            if (index === -1) {
                const update = [...selectedItems, selectedItem];
                this.props.onChange(update);
                return { selectedItems: update };
            }

            const selectedItemsCopy = selectedItems.slice();
            selectedItemsCopy.splice(index, 1);
            this.props.onChange(selectedItemsCopy);
            return { selectedItems: selectedItemsCopy };
        });
    };

    onSingleSelect = (item) => {
        this.setState(({ selectedItem }) => {
            if (selectedItem === item) {
                this.props.onChange(null);
                return { selectedItem: null };
            }
            this.props.onChange(item);
            return { selectedItem: item };
        });
    };

    render() {
        const { multi } = this.props;
        const { selectedItems, selectedItem } = this.state;

        const selected = multi ? selectedItems : selectedItem;
        const functions = { onSelect: multi ? this.onMultiSelect : this.onSingleSelect };

        return this.props.children(selected, functions);
    }
}

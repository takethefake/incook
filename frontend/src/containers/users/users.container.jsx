import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import CookItem from "../../components/cookItem/cookItem.compnent";
import { ID_KEY } from "../../actions/commons";
import { getAllUsers } from "../../actions";

class Users extends Component {
  componentDidMount() {
    const { dispatch } = this.props;
    dispatch(getAllUsers());
  }

  render() {
    const { users } = this.props;
    return (
      <section
        style={{
          display: "flex",
          alignItems: "center",
          flexDirection: "column"
        }}
      >
        <h2>Cooks</h2>
        {users
          .sort(
            ({ dishCount: a }, { dishCount: b }) => (a > b ? -1 : a < b ? 1 : 0)
          )
          .map(user => <CookItem key={user[ID_KEY]} {...user} />)}
      </section>
    );
  }
}

Users.propTypes = {
  users: PropTypes.array
};

Users.defaultProps = {
  users: []
};

export default connect(
  ({ users }) => ({ users: users.all }),
  dispatch => ({ dispatch })
)(Users);

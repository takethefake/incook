import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { TileContainer, Tile } from "../../components/styled.component";
import DishItem from "../../components/dishItem/dishItem.component";
import EditDishItem from "../../components/editDish/editDishItem.component";
import DishItemHolder from "../../components/dishItem/dishItemHolder.component";
import { ID_KEY } from "../../actions/commons";
import {
  getAllDishes,
  joinDish,
  leaveDish,
  deleteDish,
  editDish
} from "../../actions";
import moment from "moment";

const groupByDay = (acc, dish) => {
  const lastWeekDay = acc[acc.length - 1];
  if (!Array.isArray(lastWeekDay)) {
    return [[dish]];
  }
  const lastDish = lastWeekDay[lastWeekDay.length - 1];
  const lastDishDate = new Date(lastDish.cookingDate);
  const date = new Date(dish.cookingDate);

  if (lastDishDate.getDay() === date.getDay()) {
    lastWeekDay.push(dish);
    return acc;
  }

  return [...acc, [dish]];
};

const sortByDate = ({ cookingDate: a }, { cookingDate: b }) => {
  const dateA = new Date(a);
  const dateB = new Date(b);

  return dateA >= dateB;
};

const removeExpiredDishes = dish => {
  const diff = moment().isSameOrBefore(dish.cookingDate, "days");
  return diff;
};

class Dishes extends Component {
  state = {
    editDish: null
  };

  componentDidMount() {
    const { dispatch } = this.props;
    dispatch(getAllDishes());
  }

  editDish = dish => {
    this.setState({
      ...this.state,
      editDish: dish[ID_KEY]
    });
  };

  onCancelEditDish = () => {
    this.setState({
      ...this.state,
      editDish: false
    });
  };

  render() {
    const { dishes } = this.props;
    const sortedDishes = dishes
      .filter(removeExpiredDishes)
      .sort(sortByDate)
      .reduce(groupByDay, []);

    //
    return (
      <TileContainer>
        {sortedDishes.map(day => {
          let tileCount = 0;
          return day.map(dish => {
            const item =
              dish[ID_KEY] === this.state.editDish ? (
                <Tile className={"cook edit"}>
                  <EditDishItem
                    dish={dish}
                    edit
                    onCancel={this.onCancelEditDish}
                    onSave={editedDish => {
                      this.props.dispatch(editDish(editedDish));
                      this.onCancelEditDish();
                    }}
                  />
                </Tile>
              ) : (
                <DishItem
                  leaveDish={() => this.props.dispatch(leaveDish(dish))}
                  joinDish={() => this.props.dispatch(joinDish(dish))}
                  deleteDish={() => this.props.dispatch(deleteDish(dish))}
                  editDish={() => {
                    this.editDish(dish);
                  }}
                  currentUser={this.props.auth.user}
                  key={dish[ID_KEY]}
                  {...dish}
                />
              );
            const tile =
              tileCount === 0 ? (
                <DishItemHolder cookingDate={dish.cookingDate}>
                  {item}
                </DishItemHolder>
              ) : (
                <DishItemHolder>{item}</DishItemHolder>
              );
            tileCount++;
            return tile;
          });
        })}
      </TileContainer>
    );
  }
}

Dishes.propTypes = {
  dishes: PropTypes.array
};

Dishes.defaultProps = {
  dishes: []
};

export default connect(
  ({ dishes, auth }) => ({ dishes: dishes.all, auth }),
  dispatch => ({ dispatch })
)(Dishes);

import React, { Component } from "react";
import { connect } from "react-redux";

import Login from "../../pages/login/login.page";
import Register from "../../pages/register/register.page";
import Dashboard from "../../pages/dashboard/dashboard.page";
import Cooks from "../../pages/cookPage/cook.page";
import NotFoundPage from "../../pages/notFoundPage/notFoundPage.page";
import Overview from "../../pages/overview/overview.page";
import Transfer from "../../pages/transferPage/transfer.page";

import { Route, Switch } from "react-router-dom";
import { push } from "react-router-redux";
import { saveForRedirect } from "../../actions/auth.actions";
import { withRouter } from "react-router";

const whitelist = ["/login", "/register", "/dashboard"];

class AuthRouter extends Component {
  componentWillMount(nextProps) {
    this.checkAuth(nextProps);
  }

  componentWillReceiveProps(nextProps) {
    this.checkAuth(nextProps);
  }

  checkAuth = (props = this.props) => {
    const { user, redirect, dispatch, history: { location } } = props;
    console.log(props.loginError);
    if (
      !user.token &&
      whitelist.indexOf(location.pathname) === -1 &&
      location.pathname
    ) {
      dispatch(saveForRedirect(location.pathname));
      dispatch(push("/login"));
    }

    if (user && user.token && redirect) {
      dispatch(push(redirect));
      dispatch(saveForRedirect(""));
    }
  };

  render() {
    const { history: { location } } = this.props;

    return (
      <Switch key={location.pathname} location={location}>
        <Route exact path="/login" component={Login} />
        <Route exact path="/register" component={Register} />
        <Route exact path="/cooks" component={Cooks} />
        <Route exact path="/dashboard" component={Dashboard} />
        <Route exact path="/transfer" component={Transfer} />
        <Route exact path="/" component={Overview} />
        <Route component={NotFoundPage} />
      </Switch>
    );
  }
}

export default withRouter(
  connect(({ auth }) => ({ ...auth }), dispatch => ({ dispatch }))(AuthRouter)
);

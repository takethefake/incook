import React, { Component } from "react";
import PropTypes from "prop-types";
import styled from "react-emotion";
import TopBar from "../../components/topBar/topBar.component";
import theme from "../../style/theme";

export const LayoutContainer = styled("div")`
  width: 100%;
  display: flex;
  flex-flow: column nowrap;
  align-items: center;
  color: ${theme.colors.text};
  background-color: ${theme.colors.background};
`;

const Main = styled("main")`
  display: flex;
  flex-direction: column;
  flex: 1;
  padding: 55px 80px 0;
  width: 100%;
  @media screen and (max-width: 500px) {
    padding: 55px 20px 0;
  }
`;

export default class Layout extends Component {
  render() {
    return (
      <LayoutContainer>
        {this.props.renderTopBarLinks ? (
          <TopBar />
        ) : (
          <TopBar renderMenuItems={() => {}} />
        )}
        />
        <Main>{this.props.children}</Main>
      </LayoutContainer>
    );
  }
}
Layout.propTypes = {
  renderTopBarLinks: PropTypes.bool
};
Layout.defaultProps = {
  renderTopBarLinks: true
};

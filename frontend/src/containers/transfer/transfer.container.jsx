import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { getAllTransactions, createTransaction } from "../../actions";

import CreateTransfer from "../../components/createTransfer";

class Transfers extends Component {
  componentDidMount() {
    const { dispatch } = this.props;
    dispatch(getAllTransactions());
  }

  render() {
    const { transactions } = this.props;
    return (
      <section
        style={{
          display: "flex",
          alignItems: "center",
          flexDirection: "column"
        }}
      >
        <h2>Transfer</h2>
        <CreateTransfer
          errorMsg={this.props.error ? this.props.error.message : ""}
          onSubmit={obj => {
            this.props.dispatch(createTransaction(obj));
          }}
        />
        <h2>Previous Transactions</h2>
        {transactions.map(trans => (
          <div>
            {new Date(trans.createdAt).toLocaleString("de-DE", {
              timeZone: "UTC"
            })}: {trans.from.username} - {trans.amount} -> {trans.to.username}
          </div>
        ))}
      </section>
    );
  }
}

Transfers.propTypes = {
  transactions: PropTypes.array
};

Transfers.defaultProps = {
  transactions: []
};

export default connect(
  ({ transactions }) => ({
    transactions: transactions.all,
    error: transactions.error
  }),
  dispatch => ({ dispatch })
)(Transfers);

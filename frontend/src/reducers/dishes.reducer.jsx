import {
  GET_DISHES,
  GET_DISHES_ERROR,
  GET_DISHES_FETCHING,
  CREATE_DISH,
  CREATE_DISH_ERROR,
  CREATE_DISH_FETCHING,
  EDIT_DISH,
  EDIT_DISH_ERROR,
  EDIT_DISH_FETCHING,
  DELETE_DISH,
  DELETE_DISH_ERROR,
  DELETE_DISH_FETCHING,
  JOIN_DISH,
  JOIN_DISH_ERROR,
  JOIN_DISH_FETCHING,
  LEAVE_DISH,
  LEAVE_DISH_ERROR,
  LEAVE_DISH_FETCHING
} from "../actions";

const initialState = {
  all: [],
  error: null,
  loading: false
};

export default (state = initialState, action) => {
  switch (action.type) {
    case GET_DISHES:
      return {
        ...state,
        all: action.dishes,
        error: null,
        loading: false
      };

    case GET_DISHES_FETCHING:
      return {
        ...state,
        loading: true
      };

    case GET_DISHES_ERROR:
      return {
        ...state,
        error: action.error,
        loading: false
      };
    case DELETE_DISH:
    case EDIT_DISH:
    case CREATE_DISH:
    case JOIN_DISH:
    case LEAVE_DISH:
      return {
        ...state,
        error: null,
        loading: false
      };
    case DELETE_DISH_FETCHING:
    case EDIT_DISH_FETCHING:
    case CREATE_DISH_FETCHING:
    case JOIN_DISH_FETCHING:
    case LEAVE_DISH_FETCHING:
      return {
        ...state,
        loading: true
      };

    case DELETE_DISH_ERROR:
    case EDIT_DISH_ERROR:
    case CREATE_DISH_ERROR:
    case JOIN_DISH_ERROR:
    case LEAVE_DISH_ERROR:
      return {
        ...state,
        error: action.error,
        loading: false
      };
    default:
      return state;
  }
};

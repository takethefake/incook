import { GET_USERS, GET_USERS_ERROR, GET_USERS_FETCHING } from "../actions";

const initialState = {
  all: [],
  error: null,
  loading: false
};

export default (state = initialState, action) => {
  switch (action.type) {
    case GET_USERS:
      return {
        ...state,
        all: action.users,
        error: null,
        loading: false
      };

    case GET_USERS_FETCHING:
      return {
        ...state,
        loading: true
      };

    case GET_USERS_ERROR:
      return {
        ...state,
        error: action.error,
        loading: false
      };
    default:
      return state;
  }
};

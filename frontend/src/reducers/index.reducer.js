import { combineReducers } from "redux";
import { routerReducer } from "react-router-redux";
import dishes from "./dishes.reducer";
import transactions from "./transactions.reducer";
import users from "./users.reducer";
import auth from "./auth.reducer";

export default combineReducers({
  router: routerReducer,
  dishes,
  transactions,
  auth,
  users
});

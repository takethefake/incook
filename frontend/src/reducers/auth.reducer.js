import {
    LOGIN_USER,
    LOGIN_USER_FETCHING,
    LOGIN_USER_ERROR,
    REGISTER_USER_FETCHING,
    REGISTER_USER_ERROR,
    LOGOUT_USER,
    SAVE_FOR_REDIRECT,
} from '../actions/auth.actions';

function getPersistedUser() {
    const userJSON = localStorage.getItem('user');
    try {
        return JSON.parse(userJSON);
    } catch (e) {
        return null;
    }
}

function removeToken() {
    const user = getPersistedUser();
    if (!user || !user.token) {
        return;
    }

    delete user.token;
    localStorage.setItem('user', JSON.stringify(user));
}

const initialState = {
    user: getPersistedUser() || {},
    redirect: '',
    loginError: '',
    loginIsFetching: false,
    registerError: '',
    registerIsFetching: false
};

export default (state = initialState, action) => {
    switch (action.type) {
        case LOGIN_USER: {
            localStorage.setItem('user', JSON.stringify(action.user));
            return {
                ...state,
                user: action.user,
                loginIsFetching: false,
            };
        }
        case LOGOUT_USER: {
            localStorage.removeItem('user');
            return {
                ...state,
                user: {},
                loginIsFetching: false,
            };
        }
        case SAVE_FOR_REDIRECT: {
            return {
                ...state,
                redirect: action.redirect
            };
        }
        case LOGIN_USER_FETCHING: {
            return {
                ...state,
                loginIsFetching: true,
            }
        }
        case LOGIN_USER_ERROR: {
            removeToken();
            return {
                ...state,
                loginError: action.error.message,
                loginIsFetching: false,
            }
        }
        case REGISTER_USER_FETCHING: {
            return {
                ...state,
                registerIsFetching: true,
            }
        }
        case REGISTER_USER_ERROR: {
            removeToken();
            return {
                ...state,
                registerError: action.error.message,
                registerIsFetching: false,
            }
        }
        default: {
            return state;
        }
    }
}
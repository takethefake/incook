import {
  GET_TRANSACTIONS,
  GET_TRANSACTIONS_ERROR,
  GET_TRANSACTIONS_FETCHING,
  CREATE_TRANSACTION,
  CREATE_TRANSACTION_FETCHING,
  CREATE_TRANSACTION_ERROR
} from "../actions";

const initialState = {
  all: [],
  error: null,
  loading: false
};

export default (state = initialState, action) => {
  switch (action.type) {
    case GET_TRANSACTIONS:
      return {
        ...state,
        all: action.transactions,
        error: null,
        loading: false
      };

    case GET_TRANSACTIONS_FETCHING:
      return {
        ...state,
        loading: true
      };

    case GET_TRANSACTIONS_ERROR:
      return {
        ...state,
        error: action.message,
        loading: false
      };
    case CREATE_TRANSACTION:
      return {
        ...state,
        error: null,
        loading: false
      };
    case CREATE_TRANSACTION_FETCHING:
      return {
        ...state,
        loading: true
      };

    case CREATE_TRANSACTION_ERROR:
      return {
        ...state,
        error: action.error,
        loading: false
      };
    default:
      return state;
  }
};

// eslint-disable-next-line import/prefer-default-export
export function logProxy(arg, ...prefix) {
    // check for promise and log promise result
    if (arg && typeof arg.then === 'function') {
        return arg
            .then(e => logProxy(e, `(resolved)] [${prefix.join(', ')}`))
            .catch((e) => {
                logProxy(e, `(rejected)] [${prefix.join(', ')}`);
                throw e;
            });
    }
    // eslint-disable-next-line no-console
    console.log(`[logProxy${prefix.length ? ` ${prefix.join(', ')}` : ''}]:`, arg);
    return arg;
}


import React, { Component } from "react";
import { connect } from "react-redux";
import Layout from "../../containers/layout/layout.container";
import Cooks from "../../containers/users/users.container";

class CooksPage extends Component {
  render() {
    return (
      <Layout>
        <Cooks />
      </Layout>
    );
  }
}

export default connect(() => ({}), dispatch => ({ dispatch }))(CooksPage);

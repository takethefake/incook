import React, { Component } from "react";
import LoginForm from "../../components/loginForm/loginForm.component";
import { LayoutContainer } from "../../containers/layout/layout.container";
import { connect } from "react-redux";
import { loginUser } from "../../actions/auth.actions";
import Logo from "../../components/logo/logo.component";
import { Button } from "../../components/styled.component";
import theme from "../../style/theme";
import { push } from "react-router-redux";

class Login extends Component {
  state = {
    username: "",
    password: "",
    errorMsg: ""
  };

  onNameChange = event => this.setState({ username: event.target.value });
  onPasswordChange = event => this.setState({ password: event.target.value });
  onLogin = () => {
    const { username, password } = this.state;
    const { dispatch } = this.props;
    dispatch(loginUser({ username, password }));
  };

  render() {
    const { username, password } = this.state;
    const { loginError, loginIsFetching, dispatch } = this.props;

    return (
      <LayoutContainer>
        <Logo subtitle={"login"} />
        <LoginForm
          onUserNameChange={this.onNameChange}
          onPasswordChange={this.onPasswordChange}
          onSubmit={this.onLogin}
          username={username}
          password={password}
          isLoading={loginIsFetching}
          errorMsg={loginError}
          renderMoreButtons={() => (
            <Button
              className={"primary"}
              onClick={() => dispatch(push("register"))}
              style={{ backgroundColor: theme.colors.secondary }}
            >
              Register
            </Button>
          )}
        />
      </LayoutContainer>
    );
  }
}

export default connect(({ auth }) => ({ ...auth }), dispatch => ({ dispatch }))(
  Login
);

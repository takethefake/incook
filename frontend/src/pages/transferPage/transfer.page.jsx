import React, { Component } from "react";
import { connect } from "react-redux";
import Layout from "../../containers/layout/layout.container";
import Transfer from "../../containers/transfer/transfer.container";

class TransferPage extends Component {
  render() {
    return (
      <Layout>
        <Transfer />
      </Layout>
    );
  }
}

export default connect(() => ({}), dispatch => ({ dispatch }))(TransferPage);

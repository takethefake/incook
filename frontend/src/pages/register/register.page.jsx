import React, { Component } from 'react';
import LoginForm from '../../components/loginForm/loginForm.component';
import { LayoutContainer } from '../../containers/layout/layout.container';
import { connect } from 'react-redux';
import { registerUser } from '../../actions/auth.actions';
import Logo from '../../components/logo/logo.component';


class Register extends Component {
    state = {
        username: '',
        password: '',
    };

    onNameChange = (event) => this.setState({ username: event.target.value });
    onPasswordChange = (event) => this.setState({ password: event.target.value });
    onLogin = () => {
        const { username, password } = this.state;
        const { dispatch } = this.props;
        dispatch(registerUser({ username, password }));
    };

    render() {
        const { username, password } = this.state;
        const { registerIsFetching, registerError } = this.props;

        return <LayoutContainer>
            <Logo subtitle={'register'}/>
            <LoginForm
                onUserNameChange={this.onNameChange}
                onPasswordChange={this.onPasswordChange}
                onSubmit={this.onLogin}
                username={username}
                password={password}
                isLoading={registerIsFetching}
                errorMsg={registerError}
                submitText="Register"
            />
        </LayoutContainer>;
    }
}

export default connect(
    ({ auth }) => ({ ...auth }),
    dispatch => ({ dispatch }),
)(Register);
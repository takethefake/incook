import React, { Component } from "react";
import { connect } from "react-redux";
import Layout from "../../containers/layout/layout.container";
import Dishes from "../../containers/dishes/dishes.container";
import styled from "react-emotion";

import Logo from "../../components/logo/logo.component";

const CenteredSection = styled("section")`
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
`;
class Dashboard extends Component {
  render() {
    return (
      <Layout renderTopBarLinks={false}>
        <CenteredSection>
          <Logo />
          <Dishes />
        </CenteredSection>
      </Layout>
    );
  }
}

export default connect(() => ({}), dispatch => ({ dispatch }))(Dashboard);

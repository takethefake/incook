import React, { Component } from 'react';
import { connect } from 'react-redux';
import styled from 'react-emotion';
import { Link } from 'react-router-dom';
import Layout from '../../containers/layout/layout.container';

const Container = styled('div')`
    display: flex;
    flex-direction: column;
    align-items: center;
`;

const Number = styled('div')`
    margin-top: 0.5em;
    color: #CCC;
    font-size: 88px;
    font-size: 25vmin;
`;

const Message = styled('div')`
    font-size: 33px;
`;

const BackText = styled('span')`
    text-decoration: none;
    cursor: pointer;
    &:hover {
        text-decoration: none;
    }
`;

class NotFoundPage extends Component {
    render() {
        return <Layout>
            <Container>
                <Number>404</Number>
                <Message>You found nothing!</Message>
                <Link to="/"><BackText>Go to home</BackText></Link>
            </Container>
        </Layout>;
    }
}

export default connect(
    () => ({}),
    dispatch => ({ dispatch })
)(NotFoundPage);
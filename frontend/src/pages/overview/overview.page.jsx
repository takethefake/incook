import React, { Component } from "react";
import { connect } from "react-redux";
import Layout from "../../containers/layout/layout.container";
import Dishes from "../../containers/dishes/dishes.container";
import styled from "react-emotion";
import { createDish } from "../../actions/dish.actions";

import Logo from "../../components/logo/logo.component";

import { Tile, Button } from "../../components/styled.component";
import EditDishItem from "../../components/editDish/editDishItem.component";

const CenteredSection = styled("section")`
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
`;
class Overview extends Component {
  constructor() {
    super();
    this.state = {
      createDish: false
    };
  }
  handleDishCreation = dish => {
    const { dispatch } = this.props;
    dispatch(createDish(dish));
    this.toggleCreateDish();
  };

  toggleCreateDish = () => {
    this.setState({
      ...this.state,
      createDish: !this.state.createDish
    });
  };

  render() {
    return (
      <Layout>
        <CenteredSection>
          <Logo />
          {this.state.createDish ? (
            <Tile className={"cook edit"}>
              <EditDishItem
                onSave={this.handleDishCreation}
                onCancel={this.toggleCreateDish}
              />
            </Tile>
          ) : (
            <Button className={"primary"} onClick={this.toggleCreateDish}>
              Create Dish
            </Button>
          )}

          <Dishes />
        </CenteredSection>
      </Layout>
    );
  }
}

export default connect(() => ({}), dispatch => ({ dispatch }))(Overview);

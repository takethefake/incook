import React from "react";
import { injectGlobal } from "emotion";
import { ConnectedRouter } from "react-router-redux";
import AuthRouter from "./containers/authRouter/authRouter.container";
import { history } from "./store";
import WebFont from "webfontloader";

WebFont.load({
  google: {
    families: ["Titillium Web"]
  }
});

injectGlobal`
    html, body, #root, #root > div {
        height: 100%;
        width: 100%;
    }
`;

const App = () => (
  <ConnectedRouter history={history}>
    <AuthRouter />
  </ConnectedRouter>
);

export default App;

import store from '../store';

export const API_HOST = process.env.NODE_ENV === 'production' ? window.location.origin : 'http://localhost:4040';
export const catchServerError = (res) => {
    console.log(res);
    if (!res.ok) {
        return res.json().then(body => {throw new Error((body && body.message) || 'Oops, did not expect that.. 1 nicer error.')});
    }
    return res;
};

export const ID_KEY = '_id';

export const getToken = () => {
    const { auth: { user } } = store.getState();
    if (!user) {
        return null;
    }
    return user.token;
};
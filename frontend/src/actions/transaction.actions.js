import { API_HOST, catchServerError, getToken} from "./commons";

export const GET_TRANSACTIONS = "transaction/GET_TRANSACTIONS::START";
export const GET_TRANSACTIONS_FETCHING =
  "transaction/GET_TRANSACTIONS::FETCHING";
export const GET_TRANSACTIONS_ERROR = "transaction/GET_TRANSACTIONS::ERROR";

export const CREATE_TRANSACTION = "transaction/CREATE_TRANSACTION::START";
export const CREATE_TRANSACTION_FETCHING =
  "transaction/CREATE_TRANSACTION::FETCHING";
export const CREATE_TRANSACTION_ERROR = "transaction/CREATE_TRANSACTION::ERROR";

const getDefaultHeaders = (additional = {}) =>
  new Headers({
    "content-type": "application/json",
    authorization: `Bearer ${getToken()}`,
    ...additional
  });

export function getAllTransactions() {
  return dispatch => {
    dispatch({ type: GET_TRANSACTIONS_FETCHING });
    fetch(`${API_HOST}/api/transactions?limit=300`, {
      headers: getDefaultHeaders()
    })
      .then(catchServerError)
      .then(res => res.json())
      .then(transactions =>
        dispatch({
          type: GET_TRANSACTIONS,
          transactions
        })
      )
      .catch(error =>
        dispatch({
          type: GET_TRANSACTIONS_ERROR,
          error
        })
      );
  };
}

export const createTransaction = transaction => {
  return dispatch => {
    dispatch({ type: CREATE_TRANSACTION_FETCHING });
    fetch(`${API_HOST}/api/transactions`, {
      method: "post",
      headers: getDefaultHeaders(),
      body: JSON.stringify(transaction)
    })
      .then(catchServerError)
      .then(res => res.json())
      .then(transaction =>
        dispatch({
          type: CREATE_TRANSACTION,
          transaction
        })
      )
      .then(() => dispatch(getAllTransactions()))
      .catch(error =>
        dispatch({
          type: CREATE_TRANSACTION_ERROR,
          error
        })
      );
  };
};

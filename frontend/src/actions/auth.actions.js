import { push } from 'react-router-redux';
import { API_HOST, catchServerError } from './commons';

export const LOGIN_USER = 'auth/LOGIN_USER';
export const LOGIN_USER_FETCHING = 'auth/LOGIN_USER::FETCHING';
export const LOGIN_USER_ERROR = 'auth/LOGIN_USER::ERROR';

export function loginUser(user) {
    return (dispatch) => {
        dispatch({ type: LOGIN_USER_FETCHING });
        fetch(`${API_HOST}/api/auth/login`, {
            method: 'post',
            headers: new Headers({ 'content-type': 'application/json' }),
            body: JSON.stringify(user),
        })
            .then(catchServerError)
            .then(res => res.json())
            .then(user => dispatch({
                type: LOGIN_USER,
                user,
            }))
            .then(dispatch(push('')))
            .catch(error => dispatch({
                type: LOGIN_USER_ERROR,
                error,
            }));
    };
}

export const REGISTER_USER_FETCHING = 'REGISTER_USER_FETCHING';
export const REGISTER_USER = 'REGISTER_USER';
export const REGISTER_USER_ERROR = 'REGISTER_USER_ERROR';

export function registerUser(user) {
    return (dispatch) => {
        dispatch({ type: REGISTER_USER_FETCHING });
        fetch(`${API_HOST}/api/users`, {
            method: 'post',
            headers: new Headers({ 'content-type': 'application/json' }),
            body: JSON.stringify(user),
        })
            .then(catchServerError)
            .then(res => res.json())
            .then(user => dispatch({
                type: REGISTER_USER,
                user,
            }))
            .then(dispatch(push('/login')))
            .catch(error => dispatch({
                type: REGISTER_USER_ERROR,
                error,
            }));
    };
}

export const LOGOUT_USER = 'LOGOUT_USER';

export function logoutUser() {
    return {
        type: LOGOUT_USER,
    };
}

export const SAVE_FOR_REDIRECT = 'SAVE_FOR_REDIRECT';

export function saveForRedirect(redirect) {
    return {
        type: SAVE_FOR_REDIRECT,
        redirect,
    };
}
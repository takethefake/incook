import { API_HOST, catchServerError, getToken } from "./commons";

export const GET_USERS = "user/GET_USERS::START";
export const GET_USERS_FETCHING = "user/GET_USERS::FETCHING";
export const GET_USERS_ERROR = "user/GET_USERS::ERROR";

export const CREATE_USER = "user/CREATE_USER::START";
export const CREATE_USER_FETCHING = "user/CREATE_USER::FETCHING";
export const CREATE_USER_ERROR = "user/CREATE_USER::ERROR";

const getDefaultHeaders = (additional = {}) =>
  new Headers({
    "content-type": "application/json",
    authorization: `Bearer ${getToken()}`,
    ...additional
  });

export function getAllUsers() {
  return dispatch => {
    dispatch({ type: GET_USERS_FETCHING });
    fetch(`${API_HOST}/api/users`, {
      headers: getDefaultHeaders()
    })
      .then(catchServerError)
      .then(res => res.json())
      .then(users =>
        dispatch({
          type: GET_USERS,
          users
        })
      )
      .catch(error =>
        dispatch({
          type: GET_USERS_ERROR,
          error
        })
      );
  };
}

import { API_HOST, catchServerError, getToken, ID_KEY } from "./commons";

export const GET_DISHES = "dish/GET_DISHES::START";
export const GET_DISHES_FETCHING = "dish/GET_DISHES::FETCHING";
export const GET_DISHES_ERROR = "dish/GET_DISHES::ERROR";

export const CREATE_DISH = "dish/CREATE_DISH::START";
export const CREATE_DISH_FETCHING = "dish/CREATE_DISH::FETCHING";
export const CREATE_DISH_ERROR = "dish/CREATE_DISH::ERROR";

export const EDIT_DISH = "dish/EDIT_DISH::START";
export const EDIT_DISH_FETCHING = "dish/EDIT_DISH::FETCHING";
export const EDIT_DISH_ERROR = "dish/EDIT_DISH::ERROR";

export const DELETE_DISH = "dish/DELETE_DISH::START";
export const DELETE_DISH_FETCHING = "dish/DELETE_DISH::FETCHING";
export const DELETE_DISH_ERROR = "dish/DELETE_DISH::ERROR";

export const JOIN_DISH = "dish/JOIN_DISH::START";
export const JOIN_DISH_FETCHING = "dish/JOIN_DISH::FETCHING";
export const JOIN_DISH_ERROR = "dish/JOIN_DISH::ERROR";

export const LEAVE_DISH = "dish/LEAVE_DISH::START";
export const LEAVE_DISH_FETCHING = "dish/LEAVE_DISH::FETCHING";
export const LEAVE_DISH_ERROR = "dish/LEAVE_DISH::ERROR";

const getDefaultHeaders = (additional = {}) =>
  new Headers({
    "content-type": "application/json",
    authorization: `Bearer ${getToken()}`,
    ...additional
  });

export function getAllDishes() {
  return dispatch => {
    dispatch({ type: GET_DISHES_FETCHING });
    fetch(`${API_HOST}/api/dishes`, {
      headers: getDefaultHeaders()
    })
      .then(catchServerError)
      .then(res => res.json())
      .then(dishes =>
        dispatch({
          type: GET_DISHES,
          dishes
        })
      )
      .catch(error =>
        dispatch({
          type: GET_DISHES_ERROR,
          error
        })
      );
  };
}

export function joinDish(dish) {
  return dispatch => {
    dispatch({ type: JOIN_DISH_FETCHING });
    fetch(`${API_HOST}/api/dishes/${dish[ID_KEY]}/join`, {
      headers: getDefaultHeaders(),
      method: "post"
    })
      .then(catchServerError)
      .then(res => res.json())
      .then(dishes =>
        dispatch({
          type: JOIN_DISH,
          dishes
        })
      )
      .then(() => dispatch(getAllDishes()))
      .catch(error =>
        dispatch({
          type: JOIN_DISH_ERROR,
          error
        })
      );
  };
}

export function leaveDish(dish) {
  return dispatch => {
    dispatch({ type: LEAVE_DISH_FETCHING });
    fetch(`${API_HOST}/api/dishes/${dish[ID_KEY]}/join`, {
      headers: getDefaultHeaders(),
      method: "delete"
    })
      .then(catchServerError)
      .then(res => res.json())
      .then(dishes =>
        dispatch({
          type: LEAVE_DISH,
          dishes
        })
      )
      .then(() => dispatch(getAllDishes()))
      .catch(error =>
        dispatch({
          type: LEAVE_DISH_ERROR,
          error
        })
      );
  };
}

export const createDish = dish => {
  return dispatch => {
    dispatch({ type: CREATE_DISH_FETCHING });
    fetch(`${API_HOST}/api/dishes`, {
      method: "post",
      headers: getDefaultHeaders(),
      body: JSON.stringify(dish)
    })
      .then(catchServerError)
      .then(res => res.json())
      .then(dish =>
        dispatch({
          type: CREATE_DISH,
          dish
        })
      )
      .then(() => dispatch(getAllDishes()))
      .catch(error =>
        dispatch({
          type: CREATE_DISH_ERROR,
          error
        })
      );
  };
};

export const editDish = dish => {
  return dispatch => {
    dispatch({ type: EDIT_DISH_FETCHING });
    fetch(`${API_HOST}/api/dishes/${dish[ID_KEY]}`, {
      method: "put",
      headers: getDefaultHeaders(),
      body: JSON.stringify(dish)
    })
      .then(catchServerError)
      .then(res => res.json())
      .then(dish =>
        dispatch({
          type: EDIT_DISH,
          dish
        })
      )
      .then(() => dispatch(getAllDishes()))
      .catch(error =>
        dispatch({
          type: EDIT_DISH_ERROR,
          error
        })
      );
  };
};

export const deleteDish = dish => {
  return dispatch => {
    dispatch({ type: DELETE_DISH_FETCHING });
    fetch(`${API_HOST}/api/dishes/${dish[ID_KEY]}`, {
      method: "delete",
      headers: getDefaultHeaders()
    })
      .then(catchServerError)
      .then(res => res.json())
      .then(dish =>
        dispatch({
          type: DELETE_DISH,
          dish
        })
      )
      .then(() => dispatch(getAllDishes()))
      .catch(error =>
        dispatch({
          type: DELETE_DISH_ERROR,
          error
        })
      )
      .then(() => {
        dispatch(getAllDishes());
      });
  };
};

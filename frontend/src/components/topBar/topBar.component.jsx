import React from "react";
import PropTypes from "prop-types";
import styled from "react-emotion";
import { Link } from "react-router-dom";
import theme from "../../style/theme";
import { connect } from "react-redux";
import { logoutUser } from "../../actions/auth.actions";
import "font-awesome/css/font-awesome.css";

const Navbar = styled("nav")`
  display: flex;
  align-items: center;
  justify-content: space-between;
  height: 55px;
  position: fixed;
  width: 100%;
  padding: 0 15px;
  z-index: 1000;
  background-color: ${theme.colors.navBar.background};
  color: ${theme.colors.navBar.text};
`;

const Brand = styled(Link)`
  height: 100%;
  font-size: 2em;
  text-decoration: none;
  color: ${theme.colors.navBar.text};
  &:hover,
  &:focus,
  &:active,
  &:visited {
    text-decoration: none;
    color: ${theme.colors.navBar.text};
  }
  & span:first-of-type {
    font-weight: bold;
  }
`;

const MenuItem = styled(Link)`
  height: 100%;
  font-size: 1em;
  padding: 0 0.5em;
  text-decoration: none;
  color: ${theme.colors.navBar.text};
  &:hover,
  &:focus,
  &:active,
  &:visited {
    text-decoration: none;
    color: ${theme.colors.navBar.text};
  }
`;

const MenuSection = styled("section")``;

const renderMenuItems = dispatch => [
  <MenuItem key="dishes" to="/">
    Dishes
  </MenuItem>,
  <MenuItem key="cooks" to="/cooks">
    Cooks
  </MenuItem>,
  <MenuItem key="transfer" to="/transfer">
    Transfer
  </MenuItem>,
  <MenuItem key="logout" to="/login" onClick={() => dispatch(logoutUser())}>
    <i className="fa fa-sign-out" />
  </MenuItem>
];

const TopBar = ({ renderMenuItems, dispatch }) => {
  return (
    <Navbar>
      <Brand to="/" />
      <MenuSection>{renderMenuItems(dispatch)}</MenuSection>
    </Navbar>
  );
};

TopBar.propTypes = {
  renderMenuItems: PropTypes.func,
  dispatch: PropTypes.func
};

TopBar.defaultProps = {
  renderMenuItems,
  dispatch: undefined
};

export default connect(() => ({}), dispatch => ({ dispatch }))(TopBar);

import React from "react";
import PropTypes from "prop-types";
import moment from "moment";
import theme from "../../style/theme";

import styled from "react-emotion";

export const TileHeading = styled("h4")`
  border-bottom: 2px solid ${theme.colors.primary};
`;

const DishItemHolder = props => (
  <div>
    <TileHeading>
      {props.cookingDate
        ? moment(props.cookingDate).format("dddd DD. MMM")
        : ""}
    </TileHeading>
    {props.children}
  </div>
);

DishItemHolder.propTypes = {
  children: PropTypes.element.isRequired,
  cookingDate: PropTypes.string
};

export default DishItemHolder;

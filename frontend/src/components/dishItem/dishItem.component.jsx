import React from "react";
import PropTypes from "prop-types";
import styled from "react-emotion";
import theme from "../../style/theme";

import forkJoined from "./fork-joined.svg";
import forkDisabled from "./fork-disabled.svg";
import forkCook from "./fork-cook.svg";

import {
  Tile,
  Button,
  TileButtons,
  TileFooter,
  TileHover,
  TileImage,
  TileDescription,
  TileFooterRow,
  TileDishTitle
} from "../styled.component";

import { ID_KEY } from "../../actions/commons";

const Participants = styled("ul")`
  list-style: none;
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  padding: 0;
  &:before {
    color: ${theme.colors.primary};
    margin-left: -5px;
    font-size: 16px;
    font-weight: 700;
    content: "Participants";
  }
  .empty {
    color: #a6a6a6;
  }
`;

const Fork = styled("div")`
  height: 18px;
  width: 12px;
  background-size: 100% 100%;
  background-position: center center;
  margin: 6px;
  &.joined {
    background-image: url(${forkJoined});
  }
  &.cook {
    background-image: url(${forkCook});
  }
  &.free {
    background-image: url(${forkDisabled});
  }
`;

const DishItem = ({
  currentUser,
  joinDish,
  leaveDish,
  deleteDish,
  editDish,
  id,
  name,
  price,
  description,
  cookingDate,
  cook,
  link,
  maxParticipants,
  participants
}) => {
  const userIsInParticipants = participants.find(
    participant => participant._id === currentUser.id
  );

  const userIsCook = cook.id === currentUser.id;

  const hasJoinedClass = userIsInParticipants ? "joined" : "";

  const myDishes = userIsCook ? "cook" : "";

  const elementClass = myDishes + " " + hasJoinedClass;

  const emptyPlaces = [];
  let forks = [];

  forks = participants.map(participant => (
    <Fork className={userIsCook ? "cook" : "joined"} />
  ));

  if (participants.length < maxParticipants) {
    for (let i = 0; i < maxParticipants - participants.length; i++) {
      emptyPlaces.push(
        <li className={"empty"} key={i}>
          (empty)
        </li>
      );
      forks.push(<Fork className={"free"} />);
    }
  }

  return (
    <Tile className={elementClass} style={{ overflow: "hidden" }}>
      <TileHover className={"tile-hover"}>
        <TileDescription>{description}</TileDescription>
        <Participants>
          {participants.map((participant, i) => (
            <li key={participant[ID_KEY]}>
              {`${i !== 0 ? " " : ""}` + participant.username}
            </li>
          ))}
          {emptyPlaces}
        </Participants>
      </TileHover>
      <TileImage
        style={{ backgroundImage: `url(${link})` }}
        className={"tile-image"}
      />
      <TileFooter className={elementClass + " tile-footer"}>
        <TileDishTitle>{name}</TileDishTitle>
        <TileFooterRow>
          <span className={"tile-footer-row-key"}>by </span>
          <span>{cook.username}</span>
        </TileFooterRow>
        <TileFooterRow>
          <span className={"tile-footer-row-key"}>for </span>
          <span>{price}</span>
          <span className={"tile-footer-row-key"}> Point/s </span>
        </TileFooterRow>
        <TileFooterRow>{forks}</TileFooterRow>
        {userIsCook ? (
          <TileButtons className={"tile-buttons"}>
            <Button className={"error"} onClick={deleteDish}>
              Delete
            </Button>
            <Button className={"tertiary"} onClick={editDish}>
              Edit
            </Button>
          </TileButtons>
        ) : userIsInParticipants ? (
          <TileButtons className={"tile-buttons"}>
            <Button className={"error"} onClick={leaveDish}>
              Leave
            </Button>
            <Button className={"secondary"} disabled>
              Joined
            </Button>
          </TileButtons>
        ) : !!currentUser.id ? (
          <TileButtons className={"tile-buttons"}>
            <Button
              className={"secondary"}
              disabled={participants.length === maxParticipants}
              onClick={joinDish}
            >
              Join
            </Button>
          </TileButtons>
        ) : null}
      </TileFooter>
    </Tile>
  );
};

DishItem.propTypes = {
  currentUser: PropTypes.object,
  id: PropTypes.string,
  name: PropTypes.string,
  description: PropTypes.string,
  createdAt: PropTypes.string,
  cookingDate: PropTypes.string,
  cook: PropTypes.object,
  maxParticipants: PropTypes.number,
  participants: PropTypes.array,
  recipie: PropTypes.object,
  link: PropTypes.string,
  joinDish: PropTypes.func.isRequired,
  leaveDish: PropTypes.func.isRequired
};

DishItem.defaultProps = {};

export default DishItem;

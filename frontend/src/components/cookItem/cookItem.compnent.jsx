import React from "react";
import PropTypes from "prop-types";
import { css } from "emotion";
import theme from "../../style/theme";

import {
  ElementHolder,
  ElementHeading,
  Information,
  Meta,
  CookText
} from "../styled.component";

const UserItem = ({ username, dishCount }) => {
  const cookClass =
    dishCount >= 0
      ? css`
          background-color: ${theme.colors.cookPositive};
        `
      : css`
          background-color: ${theme.colors.cookNegative};
        `;
  return (
    <ElementHolder className={cookClass}>
      <Information>
        <ElementHeading>{username}</ElementHeading>
      </Information>
      <Meta>
        <CookText>{dishCount}</CookText>
      </Meta>
    </ElementHolder>
  );
};

UserItem.propTypes = {
  username: PropTypes.object,
  dishCount: PropTypes.number
};

UserItem.defaultProps = {};

export default UserItem;

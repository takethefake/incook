import React from "react";
import PropTypes from "prop-types";
import {
  Form,
  FormField,
  Button,
  ErrorLabel,
  Input,
  Center
} from "../styled.component";

export default function LoginForm({
  onUserNameChange,
  onPasswordChange,
  onSubmit,
  username,
  password,
  errorMsg,
  isLoading,
  submitText,
  renderMoreButtons
}) {
  return (
    <Form
      id="login"
      onSubmit={e => {
        e.preventDefault();
        onSubmit();
      }}
    >
      <FormField>
        <label>Username</label>
        <Input value={username} onChange={onUserNameChange} type="email" />
      </FormField>
      <FormField>
        <label>Password</label>
        <Input value={password} onChange={onPasswordChange} type="password" />
      </FormField>
      <FormField>
        <Center>
          <Button className={"primary"} for="login" disabled={isLoading}>
            {submitText}
          </Button>
          {renderMoreButtons && renderMoreButtons()}
        </Center>
      </FormField>
      <FormField>
        {errorMsg && (
          <ErrorLabel style={{ marginBottom: 10 }}>{errorMsg}</ErrorLabel>
        )}
      </FormField>
    </Form>
  );
}

LoginForm.propTypes = {
  onUserNameChange: PropTypes.func.isRequired,
  onPasswordChange: PropTypes.func.isRequired,
  onSubmit: PropTypes.func.isRequired,
  username: PropTypes.string,
  password: PropTypes.string,
  errorMsg: PropTypes.string,
  isLoading: PropTypes.bool,
  submitText: PropTypes.string,
  renderMoreButtons: PropTypes.func
};

LoginForm.defaultProps = {
  username: "",
  password: "",
  errorMsg: "",
  isLoading: false,
  submitText: "Login"
};

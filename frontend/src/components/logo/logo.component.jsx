import React, { Component } from "react";
import PropTypes from "prop-types";
import { css } from "emotion";

import logo from "./logo.svg";

const LogoClassName = css`
  display: flex;
  justify-content: center;
  align-items: center;

  height: 20vh;
  width: 100%;
  margin: 5vh 0;
  &:before {
    content: "";
    overflow: hidden;
    height: 20vh;
    width: 20vh;
    background-image: url(${logo});
    background-position: center center;
    background-repeat: no-repeat;
    background-size: 100% 100%;
  }
`;

export default class Logo extends Component {
  static propTypes = {
    subtitle: PropTypes.string
  };

  render() {
    return <div className={LogoClassName} />;
  }
}

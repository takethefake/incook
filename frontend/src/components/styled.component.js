import styled from "react-emotion";
import theme from "../style/theme";
import chroma from "chroma-js";

export const ElementHolder = styled("article")`
  display: flex;
  flex-wrap: wrap;
  justify-content: space-between;
  align-items: center;
  padding: 10px 8px;
  border-bottom: 1px solid;
  max-width: 768px;
  width: 768px;
  ${chroma(theme.colors.text)
    .darken(3)
    .hex()};
  border-right: none;
  border-left: none;
`;

export const ElementHeading = styled("h3")`
  padding: 0;
  margin: 0;
`;

export const ElementDescription = styled("p")`
  margin: 0;
`;

export const Information = styled("div")`
  flex: 0 0 25%;
`;

export const Meta = styled("div")`
  text-align: center;
`;

export const SecondaryText = styled("div")`
  color: #222;
`;
export const CookText = styled("div")`
  font-size: 20px;
  color: #222;
`;

export const Button = styled("button")`
  -webkit-appearance: none;
  outline: none;
  border: none;
  border-radius: 4px;
  padding: 4px 10px;
  min-width: 93px;
  transition: background 0.2s ease-in-out;
  &.primary {
    height: 36px;
    color: ${theme.colors.button.primary.normal.text};
    background-color: ${theme.colors.button.primary.normal.background};
    box-shadow: 1px 1px 4px rgba(0, 0, 0, 0.25);

    &:hover {
      color: ${theme.colors.button.primary.hover.text};
      background-color: ${theme.colors.button.primary.hover.background};
    }
    &:active {
      color: ${theme.colors.button.primary.active.text};
      background-color: ${theme.colors.button.primary.active.background};
    }
    &:disabled {
      color: ${theme.colors.button.primary.disabled.text};
      background-color: ${theme.colors.button.primary.disabled.background};
    }
  }
  &.secondary {
    background: none;
    color: ${theme.colors.button.secondary.normal.text};
    border: 1px solid ${theme.colors.button.secondary.normal.background};
    &:hover {
      color: ${theme.colors.button.secondary.hover.text};
      border: 1px solid ${theme.colors.button.secondary.hover.background};
    }
    &:active {
      color: ${theme.colors.button.secondary.active.text};
      border: 1px solid ${theme.colors.button.secondary.active.background};
    }
    &:disabled {
      opacity: 0.3;
      color: ${theme.colors.button.secondary.disabled.text};
      border: 1px solid ${theme.colors.button.secondary.disabled.background};
    }
  }
  &.tertiary {
    background: none;
    color: ${theme.colors.button.tertiary.normal.text};
    border: 1px solid ${theme.colors.button.tertiary.normal.background};
    &:hover {
      color: ${theme.colors.button.tertiary.hover.text};
      border: 1px solid ${theme.colors.button.tertiary.hover.background};
    }
    &:active {
      color: ${theme.colors.button.tertiary.active.text};
      border: 1px solid ${theme.colors.button.tertiary.active.background};
    }
    &:disabled {
      opacity: 0.3;
      color: ${theme.colors.button.tertiary.disabled.text};
      border: 1px solid ${theme.colors.button.tertiary.disabled.background};
    }
  }

  &.error {
    height: 24px;
    background: none;
    border: none;
    color: ${theme.colors.button.error.normal.text};
  }
`;

export const Form = styled("form")`
  display: flex;
  flex-direction: column;
  align-items: center;
  min-width: 250px;
`;

export const FormField = styled("fieldset")`
  max-width: 250px;
  z-index: 50;
  width: 100%;
  & label {
    font-size: 10px;
    display: block;
  }

  & + & {
    margin-top: 1em;
  }
`;

export const Center = styled("div")`
  display: flex;
  flex-direction: column;
  justify-content: center;
`;

export const ErrorLabel = styled("label")`
  color: ${theme.colors.error};
  border: 1px solid ${theme.colors.error};
  border-radius: 0px;
  width: 100%;
  padding: 3px 8px;
  margin-top: 1em;
`;

export const Input = styled("input")`
  background: ${theme.colors.background};
  border: none;
  outline: none;
  height: 16px;
  color: ${theme.colors.text};
  border-bottom: 1px solid
    ${chroma.mix(theme.colors.background, theme.colors.text).hex()};
  border-radius: 0px;
  width: 100%;
  padding: 3px 0px 8px;
`;

export const Textarea = styled("textarea")`
  background: ${theme.colors.background};
  border: none;
  height: 30px;
  outline: none;
  color: ${theme.colors.text};
  border-bottom: 1px solid
    ${chroma.mix(theme.colors.background, theme.colors.text).hex()};
  border-radius: 0px;
  width: 100%;
  max-width: 100%;
  padding: 3px 0px 8px;
`;

export const TileContainer = styled("div")`
  align-items: flex-end;
  justify-content: center;
  flex-wrap: wrap;
  display: flex;
  width: 100%;
  padding: 0 10%;
  @media screen and (max-width: 500px) {
    justify-content: center;
    align-items: center;
  }
`;

export const Tile = styled("div")`
  display: flex;
  position: relative;
  background-color: ${theme.colors.tile.normal.background};
  flex-direction: column;
  height: 510px;
  min-height: 510px;
  width: 320px;
  border-radius: 8px;
  box-shadow: 4px 4px 16px rgba(0, 0, 0, 0.25);
  margin-right: 40px;
  margin-bottom: 40px;

  &.joined {
    background-color: ${theme.colors.tile.joined.background};
  }

  &.cook {
    background-color: ${theme.colors.tile.cook.background};
  }

  &.edit {
    overflow: initial;
  }

  &:hover {
    .tile-hover {
      display: block;
    }
    .tile-image {
      filter: blur(10px) opacity(0.4);
    }
    .tile-footer {
      .tile-buttons {
        display: flex;
      }
    }
  }
  @media screen and (max-width: 500px) {
    width: 300px;
    margin-right: 0;
    height: 377px;
  }
`;

export const TileHover = styled("div")`
  display: none;
  position: absolute;
  border-radius: 8px;
  height: 340px;
  overflow-y: auto;
  width: 100%;
  padding: 10px 
  z-index: 10;
  top: 0;
  left: 0;
  right: 0;
`;

export const TileImage = styled("div")`
  background-size: cover;
  background-position: center center;
  min-height: 333px;
  border-radius: 8px;
`;

export const TileFooter = styled("div")`
  position: absolute;
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  border-top: 161px;
  padding: 10px;
  left: 0px;
  right: 0px;
  z-index: 20;
  border-radius: 8px;
  bottom: 0;
`;

export const TileFooterRow = styled("div")`
  display: flex;
  justify-content: space-between;
  span.tile-footer-row-key {
    font-weight: 700;
    margin-right: 5px;
    margin-left: 5px;
    font-size: 16px;
  }
`;

export const TileDishTitle = styled("h3")`
  width: 100%;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
  text-align: center;
`;

export const TileDishPoints = styled("p")`
  width: 100%;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
  height: 14px;
  font-size: 12px;
  text-align: center;
`;

export const TileButtons = styled("div")`
  form > & {
    margin-top: 17px;
  }
  justify-content: space-around;
  display: flex;
  padding: 5px 0;
  width: calc(100% - 20px);
`;

export const TileDescription = styled("div")`
  margin-bottom: 10px;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  margin-left: 5px;
  &:before {
    color: ${theme.colors.primary};
    margin-left: -5px;
    font-size: 16px;
    font-weight: 700;
    content: "Description";
  }
`;

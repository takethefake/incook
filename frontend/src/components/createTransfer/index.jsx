import React from "react";
import PropTypes from "prop-types";
import {
  Form,
  FormField,
  Button,
  ErrorLabel,
  Input,
  Center
} from "../styled.component";

class CreateTransferForm extends React.Component {
  state = {
    amount: 0,
    to: ""
  };

  handleChange = e => {
    const { value, name } = e.target;
    if (e.target.type === "number") {
      this.setState({ [name]: parseFloat(value) });
    } else {
      this.setState({ [name]: value });
    }
  };

  render() {
    return (
      <Form
        id="login"
        onSubmit={e => {
          e.preventDefault();
          this.props.onSubmit(this.state);
        }}
      >
        <FormField>
          <label>Amount</label>
          <Input
            value={this.state.amount}
            name="amount"
            onChange={this.handleChange}
            type="number"
          />
        </FormField>
        <FormField>
          <label>To</label>
          <Input
            value={this.state.to}
            name="to"
            onChange={this.handleChange}
            type="text"
          />
        </FormField>
        <FormField>
          <Center>
            <Button
              className={"primary"}
              for="login"
              disabled={this.props.isLoading}
            >
              Transfer
            </Button>
          </Center>
        </FormField>
        <FormField>
          {this.props.errorMsg && (
            <ErrorLabel style={{ marginBottom: 10 }}>
              {this.props.errorMsg}
            </ErrorLabel>
          )}
        </FormField>
      </Form>
    );
  }
}

CreateTransferForm.propTypes = {
  onUserNameChange: PropTypes.func.isRequired,
  onPasswordChange: PropTypes.func.isRequired,
  onSubmit: PropTypes.func.isRequired,
  username: PropTypes.string,
  password: PropTypes.string,
  errorMsg: PropTypes.string,
  isLoading: PropTypes.bool,
  submitText: PropTypes.string,
  renderMoreButtons: PropTypes.func
};

CreateTransferForm.defaultProps = {
  username: "",
  password: "",
  errorMsg: "",
  isLoading: false,
  submitText: "Login"
};

export default CreateTransferForm;

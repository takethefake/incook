import "react-dates/initialize";
import "react-dates/lib/css/_datepicker.css";
import React, { Component } from "react";
import {
  Form,
  FormField,
  Button,
  ErrorLabel,
  Input,
  TileButtons
} from "../styled.component";
import { connect } from "react-redux";
import { SingleDatePicker } from "react-dates";
import moment from "moment";

const createPlainDish = () => ({
  name: "",
  description: "",
  maxParticipants: 5,
  link: "",
  price: 1
});

class EditDishItem extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dish: props.dish ? props.dish : createPlainDish(),
      date: props.dish ? moment(props.dish.cookingDate) : moment()
    };
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.dish !== this.state.dish && !nextProps.error) {
      this.setState({ dish: nextProps.dish });
    }
  }

  handleChange = e => {
    const { value, name } = e.target;
    this.setState({ dish: { ...this.state.dish, [name]: value } });
  };

  handleSave = e => {
    e.preventDefault();
    const { onSave } = this.props;
    const { dish, date } = this.state;
    onSave({ ...dish, cookingDate: date.toISOString() });
  };

  handleCancel = e => {
    e.preventDefault();
    const { onCancel } = this.props;
    onCancel();
  };

  render() {
    const { dish, date } = this.state;
    const { error, edit } = this.props;

    return (
      <Form>
        <h4>{edit ? "Edit" : "Create"} Dish</h4>
        <FormField>
          <label>Name</label>
          <Input
            type="text"
            name="name"
            onChange={this.handleChange}
            value={dish.name}
          />
        </FormField>
        <FormField>
          <label>Description</label>
          <Input
            type="text"
            name="description"
            onChange={this.handleChange}
            value={dish.description}
          />
        </FormField>
        <FormField>
          <label>Bildlink</label>
          <Input
            type="text"
            name="link"
            onChange={this.handleChange}
            value={dish.link}
          />
        </FormField>
        <FormField>
          <label>Max. participants</label>
          <Input
            type="number"
            name="maxParticipants"
            onChange={this.handleChange}
            value={dish.maxParticipants}
          />
        </FormField>
        <FormField>
          <label>Points per Meal</label>
          <Input
            type="number"
            name="price"
            onChange={this.handleChange}
            value={dish.price}
          />
        </FormField>
        <FormField>
          <SingleDatePicker
            date={date}
            onDateChange={date => this.setState({ date })}
            focused={this.state.datePickerFocused}
            onFocusChange={({ focused: datePickerFocused }) =>
              this.setState({ datePickerFocused })
            }
          />
        </FormField>
        <TileButtons>
          <Button className={"error"} type="submit" onClick={this.handleCancel}>
            Cancel
          </Button>
          <Button
            className={"secondary"}
            type="submit"
            onClick={this.handleSave}
          >
            Save
          </Button>
        </TileButtons>
        <FormField>
          {error && <ErrorLabel>{error.message}</ErrorLabel>}
        </FormField>
      </Form>
    );
  }
}

export default connect(({ dishes }) => ({ error: dishes.error }))(EditDishItem);

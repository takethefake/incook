export default {
  colors: {
    navBar: {
      text: "#FFFFFF",
      background: "#F5A623"
    },
    background: "#FeFeFe",
    border: "#CCC",
    highlight: "#FFFFFF",
    primary: "#F5A623",
    text: "#000000",
    error: "#C03221",
    selected: "#EFEFEF",
    cookPositive: "#0F7173",
    cookNegative: "#C03221",
    tile: {
      normal: {
        background: "#FFFFFF"
      },
      joined: {
        background: "#FFFFFF"
      },
      cook: {
        background: "#FFFFFF"
      }
    },
    button: {
      primary: {
        normal: {
          text: "#FFFFFF",
          background: "#F5A623"
        },
        hover: {
          text: "#FFFFFF",
          background: "#EF7911"
        },
        active: {
          text: "#FFFFFF",
          background: "#F5A623"
        },
        disabled: {
          text: "#FFFFFF",
          background: "#FAD291"
        }
      },
      secondary: {
        normal: {
          text: "#F5A623",
          background: "#F5A623"
        },
        hover: {
          text: "#EF7911",
          background: "#EF7911"
        },
        active: {
          text: "#F5A623",
          background: "#F5A623"
        },
        disabled: {
          text: "#FAD291",
          background: "#FAD291"
        }
      },
      tertiary: {
        normal: {
          text: "#A6A6A6",
          background: "#A6A6A6"
        },
        hover: {
          text: "#555555",
          background: "#555555"
        },
        active: {
          text: "#A6A6A6",
          background: "#A6A6A6"
        },
        disabled: {
          text: "#FAD291",
          background: "#FAD291"
        }
      },
      error: {
        normal: {
          text: "#FF0000"
        }
      }
    }
  }
};
